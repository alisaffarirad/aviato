from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    # reset password urls
    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),

    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),

    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),

    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    # other urls
    path('login/', views.user_login, name='login'),

    path('register/', views.register, name='register'),

    path('edit/', views.edit, name='edit'),

    path('confirm_logout/', views.confirm_logout, name='confirm_logout'),

    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

    path('change_password/', views.change_password, name='change_password'),

    path('setting/', views.user_settings, name='user_settings'),

    path('users/', views.user_list, name='users'),

    path('user/<username>/', views.user_detail, name='user_detail'),

    path('users/follow/', views.user_follow, name='user_follow')
]
