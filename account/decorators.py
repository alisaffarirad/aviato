from django.shortcuts import redirect
from django.contrib import messages

def user_is_authenticated(function):
    def wrap(request, *args, **kwargs):
            user = request.user
            if user.is_authenticated:
                messages.warning(request,'You are already logged in and can not do this.')
                return redirect('mainsite:home')
            else:
                return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__ 
    wrap.__name__ = function.__name__
    return wrap
