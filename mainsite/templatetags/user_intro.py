from django import template
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from ..models import BaseItem

register = template.Library()

@register.inclusion_tag('main/user_intro.html', takes_context=True)
def user_profile_intro(context):
    request = context['request']
    posts = BaseItem.objects.all().order_by('-created').select_subclasses().filter(owner=request.user).count()
    user=None
    if request.user.is_authenticated:
        user = get_object_or_404(User, username=request.user.username)
    return {'user': user, 'post_count':posts}

    