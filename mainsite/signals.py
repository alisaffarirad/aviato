from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from .models import BaseItem

@receiver(m2m_changed, sender=BaseItem.users_like.through)
def user_like_changed(sender, instance, **kwargs):
    instance.total_likes = instance.users_like.count()
    instance.save()