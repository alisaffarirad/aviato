from django.contrib.auth.models import User
from django.db import models
from model_utils.managers import InheritanceManager
from django.conf import settings

class BaseItem(models.Model):
    objects = InheritanceManager()
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')
    description = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    users_like = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='content_liked')
    total_likes = models.PositiveIntegerField(db_index=True, default=0)
    
    def __str__(self):
        return f'{self.owner}'

class Text(BaseItem):
    file_image = models.ImageField(upload_to='images')
    content = models.TextField()

class Video(BaseItem):
    file_image = models.ImageField(upload_to='images')
    url = models.URLField()

class File(BaseItem):
    file_image = models.ImageField(upload_to='images')
    file_stuff = models.FileField(upload_to='files')

class Image(BaseItem):
    file_image = models.ImageField(upload_to='images')
