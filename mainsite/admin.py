from django.contrib import admin
from .models import Text, Video, Image, File

# Register your models here.
admin.site.register(Text)
admin.site.register(Video)
admin.site.register(Image)
admin.site.register(File)