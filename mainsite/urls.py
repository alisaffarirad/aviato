from django.urls import path
from . import views

app_name = 'mainsite'
urlpatterns = [
    path('', views.home, name='home'),

    path('detail/<model_name>/<id>/', views.content_view, name='content_detail'),

    path('create/<model_name>/', views.ContentCreateUpdateView.as_view(), name='create_content'),

    path('update/<model_name>/<id>/', views.ContentCreateUpdateView.as_view(), name='update_content'),
    
    path('delete/<model_name>/<id>/', views.delete_content , name='content_delete'),

    path('discovery/', views.discovery, name='discovery'),

    path('like', views.content_like, name='like')

]
