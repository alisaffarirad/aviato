from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.models import User
from .models import BaseItem
from django.apps import apps
from django.views.generic.base import TemplateResponseMixin, View
from django.forms.models import modelform_factory
from django.http import Http404, JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from common.decorators import ajax_required
from django.views.decorators.http import require_POST

def home(request):
    following_ids = request.user.following.values_list('id', flat=True)
    all_stuff = None
    if following_ids:
        all_stuff = BaseItem.objects.all().order_by('-created').select_subclasses().filter(owner_id__in=following_ids)
    
    return render(request, 'main/home.html', {'all_stuff': all_stuff})
    
def discovery(request):
    all_stuff = BaseItem.objects.all().order_by('-created').select_subclasses()
    return render(request, 'main/discovery.html', {'all_stuff': all_stuff})


def content_view(request, model_name, id):
    if model_name in ['image', 'text', 'video', 'file']:
        model = apps.get_model(app_label='mainsite', model_name=model_name)
        content = get_object_or_404(model, id=id)
    else:
        raise Http404

    return render(request, 'content/detail.html', {'content': content, 'request':request})

class OwnerMixin(object):
    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(owner=self.request.user)
    

class ContentCreateUpdateView(TemplateResponseMixin, View, OwnerMixin):
    model = None
    obj = None
    template_name = 'content/form.html'

    def get_model(self, model_name):
        if model_name in ['text', 'video', 'image', 'file']:
            return apps.get_model(app_label='mainsite', model_name=model_name)
        return None

    def get_form(self, model, *args, **kwargs):
        Form = modelform_factory(model, exclude=['owner', 'created', 'updated'])
        return Form(*args, **kwargs)

    def dispatch(self, request, model_name, id=None):
        self.model = self.get_model(model_name)
        if id:
            self.obj = get_object_or_404(self.model, id=id, owner=request.user)
        return super().dispatch(request, model_name, id)

    def get(self, request, model_name, id=None):
        form = self.get_form(self.model, instance=self.obj)
        return self.render_to_response({'form': form, 'object':self.obj})

    def post(self, request, model_name, id=None):
        form = self.get_form(self.model, instance=self.obj, data=request.POST, files=request.FILES)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.save()
            return redirect('mainsite:home')
        return self.render_to_response({'form':form, 'object':self.obj})

def delete_content(request, model_name, id):
    if model_name in ['text', 'video', 'image', 'file']:
        model = apps.get_model(app_label='mainsite', model_name=model_name)
    content = get_object_or_404(model, id=id, owner=request.user)
    content.delete()
    return redirect('mainsite:home')

@ajax_required
@login_required
@require_POST
def content_like(request):
    content_id = request.POST.get('id')
    action = request.POST.get('action')
    model = request.POST.get('model')

    if content_id and action and model:
        if model in ['text', 'image', 'video', 'file']:
            content_model = apps.get_model(app_label='mainsite', model_name=model)
            content = content_model.objects.get(id=content_id)
        try:
            if action == 'like':
                content.users_like.add(request.user)
            else:
                content.users_like.remove(request.user)
            return JsonResponse({'status':'ok'})
        except:
            return JsonResponse({'status':'error'})